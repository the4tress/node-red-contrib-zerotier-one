const https = require('https')

module.exports = function(RED) {
    function GetStatusNode(config) {
        RED.nodes.createNode(this,config);

        // Retrieve the config node
        this.server = RED.nodes.getNode(config.server);

        var node = this;

        node.on('input', function(msg) {
            const options = {
                hostname: this.server.domain,
                port: 443,
                path: '/api/status',
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': data.length,
                    'Authorization': 'bearer ' + this.server.apiKey
                }
            }

            const req = https.request(options, function(res) {
                res.setEncoding('utf8');
                let body = '';

                res.on('data', (d) => {
                    body += d.toString();
                }).on('end', () => {
                    const schema = JSON.parse(body);
                    msg.payload = schema;
                    node.status({
                        text: this.server.apiKey
                    })
                    node.send(msg);
                });
            });

            req.on('error', function(error) {
                console.error(error);
            });

            req.write(data);
            req.end();
        });
    }

    RED.nodes.registerType("get-status", GetStatusNode);
}


