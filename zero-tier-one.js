module.exports = function(RED) {
    function CarbonBlackCloudNode(n) {
        RED.nodes.createNode(this,n);
        this.domain = n.domain;
        this.apiKey = n.apiKey || null;
    }
    RED.nodes.registerType("zerotier-one",ZeroTierOneNode);
}